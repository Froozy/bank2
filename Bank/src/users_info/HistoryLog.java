package users_info;

import operiatons.EnumOperation;

public class HistoryLog {
	String Title;
	EnumOperation OperationType;
	double amount = 0;
	public HistoryLog(String Title,EnumOperation OperationType,double amount){
		this.Title = Title;
		this.OperationType = OperationType;
		this.amount = amount;
	}
	public String getTitle() {
		return Title;
	}
	public EnumOperation getOperationType() {
		return OperationType;
	}
	public double getAmount() {
		return amount;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public void setOperationType(EnumOperation OperationType) {
		this.OperationType = OperationType;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
