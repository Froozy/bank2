package users_info;

import operiatons.Operation;

public class Account {
	private String number;
	public History history;
	private double amount;
	public Account(String number,double amount)
	{
		this.number = number;
		history = new History();
		this.amount = amount;
	}
	
	public String getNumber() {
		return number;
	}
	public History getHistory() {
		return history;
	}
	public double getAmount() {
		return amount;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public void setHistory(History history) {
		this.history = history;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public void add(double amount)
	{
		this.amount += amount;
	}
	public void subtract(double amount)
	{
		this.amount -= amount;
	}
}
