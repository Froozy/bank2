package users_info;
import java.util.ArrayList;
import java.util.List;

import operiatons.EnumOperation;

public class History {
	public List<HistoryLog> HistoryList;
	
	public void addLog(HistoryLog x)
	{
		this.HistoryList.add(x);
	}
	
	public HistoryLog addLogIncome(Account acc,double amount)
	{
		HistoryLog Log1 = new HistoryLog(acc.getNumber(),EnumOperation.Income,amount);
		return Log1;
	}
	public HistoryLog addLogsTransferFrom(Account acc,Account acc2,double amount)
	{
		HistoryLog Log2 = new HistoryLog(acc.getNumber(),EnumOperation.Transfer,-amount);
		return Log2;
	}
	public HistoryLog addLogsTransferTo(Account acc,Account acc2,double amount)
	{
		HistoryLog Log2 = new HistoryLog(acc.getNumber(),EnumOperation.Transfer,amount);
		return Log2;
	}
}
