import users_info.Account;
import users_info.History;
import users_info.HistoryLog;


public class Main {

	public static void main(String[] args) {
		Bank bank = new Bank();
		Account accA = new Account("Anna",4200);
		Account accB = new Account("Bogdan",500);
		Account accC = new Account("Cezar",51000);
		bank.income(accA,800);
		bank.transfer(accC, accB, 2000);
		accA.getAmount();
		
		

	}

}
