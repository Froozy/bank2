package operiatons;

import users_info.Account;

public class Income extends Operation {
	public Income (double amount)
    {
    	this.amount = amount;
    }
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
    public void execute(Account acc,Income x)
    {
    	acc.add(x.amount);
    }
}
