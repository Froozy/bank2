import operiatons.Income;
import operiatons.Transfer;
import users_info.Account;
import users_info.HistoryLog;


public class Bank {
	
	public void income(Account acc, double amount)
	{
		Income income = new Income(amount);
		System.out.println("Account "+acc.getNumber()+" got "+amount+"$");
		income.execute(acc, income);
		HistoryLog x = new HistoryLog(null,null,0);
		x = acc.history.addLogIncome(acc,amount);
		acc.history.addLog(x);
		acc.history.HistoryList.add(x);
	}
	
	public void transfer(Account acc1,Account acc2,double amount)
	{
		System.out.println("Account "+acc1.getNumber()+" tranfer "+amount+"$ to "+"account "+acc2.getNumber());
		acc1.history.HistoryList.add(acc1.history.addLogIncome(acc1,amount));
		acc2.history.HistoryList.add(acc2.history.addLogIncome(acc2,amount));
	}
	
}
